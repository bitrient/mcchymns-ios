//
//  HymnViewHeaderCell.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 06/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class HymnViewHeaderCell: UITableViewCell {
    
    @IBOutlet weak var leftDivider: UIImageView!
    @IBOutlet weak var rightDivider: UIImageView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var stanzaNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initDividers()
    }

    func configure(stanzaNumber: Int16, textColor: UIColor, backgroundColor: UIColor) {
        self.stanzaNumber.text = String(stanzaNumber)
        
        leftDivider.backgroundColor = textColor
        rightDivider.backgroundColor = textColor
        self.stanzaNumber.textColor = textColor
        background.backgroundColor = backgroundColor
    }
    
    func initDividers() {
        let leftDividerImage = UIImage(named: "Left Curly Divider")
        let tintedLDI = leftDividerImage?.withRenderingMode(.alwaysTemplate)
        leftDivider.image = tintedLDI
        leftDivider.tintColor = UIColor.red
        
        let rightDividerImage = UIImage(named: "Right Curly Divider")
        let tintedRDI = rightDividerImage?.withRenderingMode(.alwaysTemplate)
        rightDivider.image = tintedRDI
        rightDivider.tintColor = UIColor.red
    }
}
