//
//  ColorCell.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 09/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class ColorCell: UICollectionViewCell {
    
    @IBOutlet weak var color: UIView!
    @IBOutlet weak var checkmark: UIImageView!
    
    func configure(color: UIColor) {
        self.color.backgroundColor = color
        let check = UIImage(named: "Checkmark")
        let tintedCheck = check?.withRenderingMode(.alwaysTemplate)
        checkmark.image = tintedCheck
        checkmark.tintColor = color
        
        self.check(selected: isSelected, color: color)
    }
    
    func check(selected: Bool, color: UIColor) {
        
        if selected {
            var white: CGFloat = 0
            color.getWhite(&white, alpha: nil)
            
            if white > 0.9 {
                checkmark.tintColor = UIColor.gray
            } else {
                checkmark.tintColor = UIColor.white
            }
        } else {
            checkmark.tintColor = color
        }
    }
}
