//
//  AcknowledgementsVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 08/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class LicensesVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}
