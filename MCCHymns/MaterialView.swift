//
//  MaterialView.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 07/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

private var materialKey = false
private var roundKey = false
extension UIView {
    
    @IBInspectable var materialDesign: Bool {
        get {
            return materialKey
        }
        
        set {
            materialKey = newValue
            
            if materialKey {
                self.layer.masksToBounds = false
                self.layer.cornerRadius = 3.0
                self.layer.shadowOpacity = 0.8
                self.layer.shadowRadius = 3.0
                self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                self.layer.shadowColor = UIColor.init(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0).cgColor
            } else {
                self.layer.cornerRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.shadowRadius = 0
                self.layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var roundView: Bool {
        get {
            return roundKey
        }
        
        set {
            roundKey = newValue
            
            if roundKey {
                self.layer.masksToBounds = false
                self.layer.cornerRadius = self.layer.frame.size.width / 2
            } else {
                self.layer.cornerRadius = 0
            }
        }
    }


}
