//
//  SettingsVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 08/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class SettingsVC: UITableViewController {

    @IBOutlet weak var textSize: UILabel!
    @IBOutlet weak var textStyle: UILabel!
    @IBOutlet weak var textColor: UIView!
    @IBOutlet weak var backgroundColor: UIView!
    @IBOutlet weak var showFavoriteSwitch: UISwitch!
    @IBOutlet weak var useBackgroundTexture: UISwitch!

    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let favorites = defaults.object(forKey: Preference.startWithFavorites) as? Bool {
            showFavoriteSwitch.isOn = favorites
        }
        
        if let texture = defaults.object(forKey: Preference.useBackgroundTexture) as? Bool {
            useBackgroundTexture.isOn = texture
        }
        
        if let selectedSizeIndex = defaults.object(forKey: Preference.textSize) as? Int {
            textSize.text = Preference.textSizes[selectedSizeIndex]
        }
        
        if let selectedStyleIndex = defaults.object(forKey: Preference.textStyle) as? Int {
            textStyle.text = Preference.styles[selectedStyleIndex]
        }
        
        if let tColor = defaults.object(forKey: Preference.textColor) as? Int {
            textColor.backgroundColor = Preference.colors[tColor]
        }
        
        if let bgColor = defaults.object(forKey: Preference.backgroundColor) as? Int {
            backgroundColor.backgroundColor = Preference.colors[bgColor]
        }
    }

    @IBAction func showFavoritesPressed(_ sender: Any) {
        let favoritesSwitch = sender as! UISwitch
        
        defaults.set(favoritesSwitch.isOn, forKey: Preference.startWithFavorites)
    }
    @IBAction func showTexturePressed(_ sender: Any) {
        let textureSwitch = sender as! UISwitch
        defaults.set(textureSwitch.isOn, forKey: Preference.useBackgroundTexture)
    }
}
