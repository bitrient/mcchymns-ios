//
//  HymnHeaderCell.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 05/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class HymnHeaderCell: UITableViewCell {

    @IBOutlet weak var _subject: UILabel!
    @IBOutlet weak var _topic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(subject: String, topic: String) {
        _subject.text = subject
        _topic.text = topic
    }
}
