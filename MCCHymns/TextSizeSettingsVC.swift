//
//  TextSizeVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 08/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class TextSizeSettingsVC: UITableViewController {
    
    
    let textSizes = ["Small": 12, "Normal": 13, "Medium": 14, "Large": 15, "Extra Large": 16, "Huge": 17]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let selectedSizeIndex = UserDefaults.standard.object(forKey: Preference.textSize) as? Int {
         
            let selectedIndexPath = IndexPath(row: selectedSizeIndex, section: 0)
            tableView.selectRow(at: selectedIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
            
            tableView(tableView, didSelectRowAt: selectedIndexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            UserDefaults.standard.set(indexPath.row, forKey: Preference.textSize)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }

}
