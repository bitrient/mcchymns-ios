//
//  HymnViewCell.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 06/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class HymnViewCell: UITableViewCell {

    @IBOutlet weak var stanzaLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(stanzaNumber: Int16, textColor: UIColor) {
        stanzaLbl.text = String(stanzaNumber)
        stanzaLbl.textColor = textColor
    }

}
