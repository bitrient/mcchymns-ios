//
//  MainVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 10/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class MainVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.bool(forKey: Preference.startWithFavorites) {
            self.selectedIndex = 1
        }
    }
}
