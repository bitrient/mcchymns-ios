//
//  Hymn.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 04/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import Foundation

class HymnTest {
    private var _number: Int!
    private var _firstLine: String!
    
    var number: Int {
        return _number
    }
    
    var firstLine: String {
        return _firstLine
    }
    
    init(number: Int, firstLine: String ) {
        _number = number
        _firstLine = firstLine
    }
}
