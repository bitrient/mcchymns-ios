//
//  HymnListCell.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 04/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class HymnCell: UITableViewCell {

    @IBOutlet weak var hymnNumber: UILabel!
    @IBOutlet weak var hymnFirstLine: UILabel!
    @IBOutlet weak var favoriteImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(hymn: HymnTest) {
        hymnNumber.text = String(hymn.number)
        hymnFirstLine.text = hymn.firstLine
        favoriteImg.isHidden = false
    }
    
    func configure(hymn: Hymn) {
        hymnNumber.text = String(hymn.number)
        hymnFirstLine.text = hymn.firstLine
        favoriteImg.isHidden = !hymn.favorite
    }


}
