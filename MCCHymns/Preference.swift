//
//  Preference.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 09/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

struct Preference {
//    Hymn Preference Keys
    static let startWithFavorites: String = "showFavorites"
    static let useBackgroundTexture: String = "useBackgroundTexture"
    static let backgroundColor: String = "backgroundColor"
    static let textColor: String = "textColor"
    static let textStyle: String = "textStyle"
    static let textSize: String = "textSize"
    
    static let textSizes = ["Small", "Normal", "Medium", "Large", "Extra Large", "Huge"]
    
    static let colors = [UIColor.red, UIColor.black, UIColor.blue, UIColor.green, UIColor.gray, UIColor.darkGray, UIColor.cyan, UIColor.brown, UIColor.yellow, UIColor.magenta, UIColor.purple, UIColor.orange, UIColor.white]
    
    static let styles = ["Rancho", "Lilac Malaria", "Andada", "Inconsolata", "Underwood", "Calligraffiti"]
    
    static let initialised: String = "initialised"
    
}
