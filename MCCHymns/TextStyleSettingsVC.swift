//
//  TextStyleSettingsVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 08/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class TextStyleSettingsVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let selectedStyleIndex = UserDefaults.standard.object(forKey: Preference.textStyle) as? Int {
            
            let selectedIndexPath = IndexPath(row: selectedStyleIndex, section: 0)
            tableView.selectRow(at: selectedIndexPath, animated: true, scrollPosition: .none)
            tableView(tableView, didSelectRowAt: selectedIndexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath ) {
            cell.accessoryType = .checkmark
            UserDefaults.standard.set(indexPath.row, forKey: Preference.textStyle)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }

}
