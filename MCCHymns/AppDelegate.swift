//
//  AppDelegate.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 27/11/2016.
//  Copyright © 2016 Bitrient Services. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        initialize()
        // Override point for customization after application launch.
        
//        let tintColor: UIColor = UIColor(red: 201/255, green: 0/255, blue: 74/255, alpha: 1.0)
        let darkTintColor: UIColor = UIColor(red: 163/255, green: 0/255, blue: 60/255, alpha: 1.0)
//        self.window?.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
//        UINavigationBar.appearance().barTintColor = tintColor
        UINavigationBar.appearance().isOpaque = true
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: darkTintColor]
        
        UIBarButtonItem.appearance().tintColor = darkTintColor
        
//        Navigation Back Button
        UINavigationBar.appearance().tintColor = darkTintColor
        return true
    }
    
    func initialize() {
        
        // Set default values for preferences
        if !UserDefaults.standard.bool(forKey: Preference.initialised) {
            UserDefaults.standard.set(false, forKey: Preference.startWithFavorites)
            UserDefaults.standard.set(false, forKey: Preference.useBackgroundTexture)
            UserDefaults.standard.set("Normal", forKey: Preference.textSize)
            UserDefaults.standard.set("Lilac Malaria", forKey: Preference.textStyle)
            UserDefaults.standard.set(1, forKey: Preference.textColor)
            UserDefaults.standard.set(4, forKey: Preference.backgroundColor)
            
            UserDefaults.standard.set(true, forKey: Preference.initialised)
            
            print("App INit Yeah")
        } else {
            print("APP Init Nah")
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MCCHymns")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


}

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let context = appDelegate.persistentContainer.viewContext
