//
//  TextColorSettingsVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 09/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ColorCell"
private let footerIdentifier = "ColorCollectionFooter"
private let headerIdentifier = "ColorCollectionHeader"

class ColorsSettingsVC: UICollectionViewController {
    
    let sectionTitles = ["Background Color", "Text Color"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.allowsMultipleSelection = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedColorIndex = UserDefaults.standard.object(forKey: Preference.backgroundColor) as? Int {
            
//            let selectedPath = IndexPath(row: selectedColorIndex, section: 0)
//            collectionView?.selectItem(at: selectedPath, animated: false, scrollPosition: UICollectionViewScrollPosition.top)
//            self.collectionView(collectionView!, didSelectItemAt: selectedPath)
        }
        
        if let selectedColorIndex = UserDefaults.standard.object(forKey: Preference.textColor) as? Int {
//            let selectedPath = IndexPath(row: selectedColorIndex, section: 1)
//            collectionView?.selectItem(at: selectedPath, animated: false, scrollPosition: UICollectionViewScrollPosition.top)
//            self.collectionView(collectionView!, didSelectItemAt: selectedPath)
        }
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Preference.colors.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ColorCell {
            
            cell.configure(color: Preference.colors[indexPath.row])
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedRows: [IndexPath] = collectionView.indexPathsForSelectedItems!
        for pSelected in selectedRows { //previously selected
            if (pSelected.section == indexPath.section) && (pSelected.row != indexPath.row) {
                collectionView.deselectItem(at: pSelected, animated: false)

                self.collectionView(self.collectionView!, didDeselectItemAt: pSelected)
            }
        }
        
        if let cell = collectionView.cellForItem(at: indexPath) as? ColorCell {
            cell.check(selected: cell.isSelected, color: Preference.colors[indexPath.row])
        }
        updateSelection()
    }
    
    func updateSelection() {
        let selectedRows: [IndexPath] = collectionView!.indexPathsForSelectedItems!
        
        for selected in selectedRows {
            switch selected.section {
            case 0:
                UserDefaults.standard.set(selected.row, forKey: Preference.backgroundColor)
                break
            default:
                UserDefaults.standard.set(selected.row, forKey: Preference.textColor)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? ColorCell {
            cell.check(selected: cell.isSelected, color: Preference.colors[indexPath.row])
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var reusableView: UICollectionReusableView? = nil
        
        if kind == UICollectionElementKindSectionHeader {
            
            let reusableHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerIdentifier, for: indexPath) as! ColorCollectionReusableHeaderView
            reusableHeader.configure(title: sectionTitles[indexPath.section])
            
            reusableView = reusableHeader
        } else if kind == UICollectionElementKindSectionFooter {
            
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerIdentifier, for: indexPath)
        }
        
        return reusableView!
    }
}
