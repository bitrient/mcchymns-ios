//
//  ColorCollectionReusableView.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 09/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class ColorCollectionReusableHeaderView: UICollectionReusableView {
 
    @IBOutlet weak var titleLbl: UILabel!
    func configure(title: String) {
        titleLbl.text = title
    }
}
