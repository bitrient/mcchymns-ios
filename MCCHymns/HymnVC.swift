//
//  HymnVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 03/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit

class HymnVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    private var _selectedHymn: Hymn!
    let favoriteOff = UIImage(named: "Nav Bar Favorite Off")
    let favoriteOn = UIImage(named: "Nav Bar Favorite On")
    
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoriteBarBtn: UIBarButtonItem!
    var selectedHymn: Hymn {
        get {
            return _selectedHymn
        } set {
            _selectedHymn = newValue
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.backgroundColor = UIColor.white
        
        
        // remove text from back button
        if let topItem = navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        let title = "\(_selectedHymn.number) - \(_selectedHymn.firstLine!)"
        let index = title.index(title.startIndex, offsetBy: 25)
        navigationItem.title = title.substring(to: index) + "..."
        print("\(_selectedHymn.number) - \(_selectedHymn.firstLine!)")

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _selectedHymn.favorite {
            favoriteBarBtn.image = favoriteOn
        }
        
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 201/255, green: 0/255, blue: 74/255, alpha: 1.0)
//        
//        toolbar.barTintColor = UIColor(red: 201/255, green: 0/255, blue: 74/255, alpha: 1.0)
//        toolbar.tintColor = UIColor.cyan
//        if let tb = self.navigationController?.toolbar {
//            tb.tintColor = UIColor.black
//            tb.barTintColor = UIColor.green
//            print("ye{")
//        }
    }

    @IBAction func favoriteBarBtnPressed(_ sender: Any) {
        if _selectedHymn.favorite {
            _selectedHymn.favorite = false
            favoriteBarBtn.image = favoriteOff
        } else {
            _selectedHymn.favorite = true
            favoriteBarBtn.image = favoriteOn
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HymnViewCell", for: indexPath) as? HymnViewCell {
            
//            cell.configure(stanzaNumber: <#T##Int16#>, textColor: <#T##UIColor#>)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HymnViewHeaderCell") as! HymnViewHeaderCell
//        headerCell.configure(stanzaNumber: <#T##Int16#>, textColor: <#T##UIColor#>)
        headerCell.stanzaNumber.text = String(section + 1)
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        return 35
    }
    
    var indexArray = ["Section 1", "Section 2", "Section 3", "Section 4"]
    func numberOfSections(in tableView: UITableView) -> Int {
        return indexArray.count
    }
    
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        return indexArray
//    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return indexArray[section]
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
}
