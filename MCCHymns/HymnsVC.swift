//
//  HymnsVC.swift
//  MCCHymns
//
//  Created by Cinfwat Dogak on 04/03/2017.
//  Copyright © 2017 Bitrient Services. All rights reserved.
//

import UIKit
import CoreData

class HymnsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentedContol: UISegmentedControl!
    
    var fetchedResultsController: NSFetchedResultsController<Hymn>!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 45
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.allowsMultipleSelectionDuringEditing = false
        print("hellow")
        
//        generateTestData()
        attemptFetch()
        
    }
    
    func attemptFetch() {
        let fetchRequest: NSFetchRequest<Hymn> = Hymn.fetchRequest()
        let numberSort: NSSortDescriptor = NSSortDescriptor(key: "number", ascending: true)
//        let firstLineSort: NSSortDescriptor = NSSortDescriptor(key: "firstLine", ascending: true)
        fetchRequest.sortDescriptors = [numberSort]

//        let hymn = Hymn(context: context!)
//        hymn.

//        fetch data
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        self.fetchedResultsController = controller
        
        do {
//            let result = try context!.fetch(Hymn.fetchRequest())
//            
//            _ = result as! [Hymn]
            try controller.performFetch()
        } catch {
            let error = error as NSError
            print("Error: \(error.debugDescription)")
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        case .update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRow(at: indexPath) as! HymnCell
                configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
            }
            break
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        let resultsController = UITableViewController(style: .plain)
        let searchController = UISearchController(searchResultsController: resultsController)
        searchController.searchBar.placeholder = "Search"
//        searchController.searchBar.tintColor = UIColor.init(red: CGFloat(201) / 255.0, green: CGFloat(0) / 255.0, blue: CGFloat(74) / 255.0, alpha: CGFloat(1))
        
        searchController.searchBar.tintColor = UIColor.darkGray
        
        self.present(searchController, animated: true, completion: nil)
    }

  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HymnCell", for: indexPath) as? HymnCell {
            
            configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
            return cell
        } else {
            //TODO: remove
            return UITableViewCell()
        }
    }
    
    func configureCell(cell: HymnCell, indexPath: NSIndexPath) {
        let hymn = fetchedResultsController.object(at: indexPath as IndexPath)
        cell.configure(hymn: hymn)
    }
    
    // rename action
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Remove"
    }
    
    // enable the editing
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // do delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            
        }
    }
    
    //
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Edit", handler: {
            (action, indexPath) in print("Edit tapped")
        })
        editAction.backgroundColor = UIColor.lightGray
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Remove", handler: {
            (action, indexPath) in print("Remove tapped")
        })
        deleteAction.backgroundColor = UIColor.red
        
        return [editAction, deleteAction]
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let sections = fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let objs = fetchedResultsController.fetchedObjects, objs.count > 0 {
            let hymn = objs[indexPath.row]
            performSegue(withIdentifier: "HymnVC", sender: hymn)
        }
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "Section \(section)"
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HymnHeaderCell") as! HymnHeaderCell
//        headerCell.configure(subject: <#T##String#>, topic: <#T##String#>)
        
        return headerCell
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 45
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (fetchedResultsController.sections?.count)!
    }
    
    func generateTestData() {
       
        for number in 1...12 {
            let hymn = Hymn(context: context)
            hymn.firstLine = "Praise, my soul, the King of Heaven;"
            hymn.number = Int16(number * 100)
            
            if number % 4 == 0 {
                hymn.favorite = true
            } else {
                hymn.favorite = false
            }
        }
        appDelegate.saveContext()
        
//        delete data 

//        do {
//            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Hymn")
//            let request = NSBatchDeleteRequest(fetchRequest: fetch)
//
//            try context.execute(request)
//        } catch {
//            print("error")
//        }
 
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "HymnVC" {
            if let destination = segue.destination as? HymnVC {
                if let selectedHymn = sender as? Hymn {
                    destination.selectedHymn = selectedHymn
                }
            }

        }
    }

}
